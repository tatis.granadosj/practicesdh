package Main;

public class Person {
    private String name, birthDate;

    //construct
    public Person(String personName, String birthDatePerson){
        name = personName;
        birthDate = birthDatePerson;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

}
